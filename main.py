import os
import cv2
import numpy as np
from keras.models import load_model
from fastapi import FastAPI, File, UploadFile
from PIL import Image
from io import BytesIO

app = FastAPI()


project_root = os.path.dirname(os.path.dirname(__file__))
output_path = os.path.join(project_root, 'model', 'Dogs-vs-Cats_model.h5')
model = load_model(output_path)


def load_image_into_numpy_array(data):
    return np.array(Image.open(BytesIO(data)))


@app.post('/')
async def root(image: UploadFile = File(...)):
    image = load_image_into_numpy_array(await image.read())
    rescaled = cv2.resize(image, (256, 256))/255.0

    processed_images = np.array([rescaled])

    predictions = model.predict(processed_images)[0]

    if predictions[0] > 0.5:
        prediction = 'Dog'
        confidence = '{:5.2f}%'.format(200*(predictions[0]-0.5))
    else:
        prediction = 'Cat'
        confidence = '{:5.2f}%'.format(200*(0.5-predictions[0]))

    return {
        'version': os.environ.get('VERSION', ''),
        'Result': prediction,
        'Confidence': confidence
    }
