FROM python:3.9-buster

WORKDIR /app

COPY . .

RUN apt update
RUN apt install libgl1-mesa-glx -y
RUN apt-get install libhdf5-dev -y
RUN pip install --upgrade pip setuptools wheel
RUN cp -r model /model
RUN pip install -r requirements.txt

EXPOSE 9000

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "9000"]